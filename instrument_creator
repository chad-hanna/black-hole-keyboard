#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import math
import optparse
import os
import wave
from multiprocessing import Pool

import rd_utils


def lmn(modes):
    l = []
    m = []
    n = []
    cnt = 0
    for i in range(2,5):
        for j in list(range(-i, i+1))[::-1]:
            for k in range(0,3):
                cnt+=1
                if cnt in modes:
                    l.append(i)
                    m.append(j)
                    n.append(k)
    return l, m, n



__version__ = 2.0
__date__    = "2022/14/01"

def parse_commandline():
    """@parse the options given on the command-line.
    """
    parser = optparse.OptionParser(usage=__doc__,version=__version__)

    parser.add_option("-s", "--spin", default=0.999999, type="float", help="Spin of black hole.  Between 0 and 1. The bigger the number the longer the tone.")
    parser.add_option("-f", "--freqsample", default=44100, type="float", help="Sample rate. Probably shouldn't change this")
    parser.add_option("-m", "--mass", default=None, type="float", help="Mass for blackhole. Ignores spin.")
    parser.add_option("-d", "--duration", default=60, type="float", help="Max duration for .wav file.")
    parser.add_option("-a", "--ampfile", default="default_amp.txt", type="string", help="User defined amplitude file.")
    parser.add_option("-o", "--output", default="WavFiles", type="string", help="Output directory for .wav files.")
    parser.add_option("-p", "--plot", default=0, type="float", help="Plots the specified frequency.")
    parser.add_option("-v", "--verbose", action="store_true", help="Be verbose in printing.")
    parser.add_option("", "--key-start", default=1, type="int", help="starting key. Default 1. If you want just middle C do 39.")
    parser.add_option("", "--key-stop", default=88, type="int", help="starting key. Default 88. If you want just middle C do 39.")
    parser.add_option("", "--mode", type="int", action="append", help="mode number between 1 and 64. Can be given multiple times. Must be ging in pairs with --amp. If omitted, use default_amp.txt")
    parser.add_option("", "--amp", type="float", action="append", help="mode amplitude. Can be given multiple times. Must be given in pairs with --mode. If omitted, use default_amp.txt")
    parser.add_option("--perfect-bell", action="store_true")

    parser.add_option("--drums", action="store_true")
    parser.add_option("--bells", action="store_true")

    opts, args = parser.parse_args()
    assert (opts.mode is None and opts.amp is None or (len(opts.mode) == len(opts.amp)))
    
    return opts 

opts = parse_commandline()
bh = rd_utils.BHI()

#Load amplitudes for modes
if opts.perfect_bell:
    l,m,n,Amp, _, _ = bh.perfect_bell(100, opts.spin)
elif opts.mode is not None:
    if len(opts.mode) != len(opts.amp):
        print("Please provide equal number of modes and amps")
        sys.exit(1)
    l, m, n = lmn(opts.mode)
    Amp = opts.amp
else:
    l, m, n, Amp = np.loadtxt(opts.ampfile, skiprows = 2, unpack=True)
amp_file_contents = {(x,y,z):a for x,y,z,a in zip(l,m,n,Amp) if a}
max_mode = max(amp_file_contents, key=amp_file_contents.get)

# Find fundamental mass for 440 Hz and given spin
if not opts.mass:
    opts.mass = bh.mass_given_spin(opts.spin)[max_mode]
if opts.verbose:
    fund_f = bh.freq_given_mass(opts.mass, opts.spin)[max_mode]
    print(f"Mass: {opts.mass}")
    print(f"Fundemental frequency at mass: {fund_f}")

# Setup BH class to generate waveforms
bh.setup(opts.spin, opts.mass, amp_file_contents, max_mode)

# Show plot of waveform if a frequency was provided
if (opts.plot > 0):
    print(f"Plotting frequency: {opts.plot}")
    count = int(12*math.log(opts.plot/440, 2))+49
    print(f"Key number: {count}")
    t, waveform = bh.waveform(count, opts.duration, opts.freqsample, opts.verbose)
    plt.plot(t, waveform)
    plt.show()
    sys.exit(1)

# Assure output directory exists
if not os.path.isdir(opts.output):
    os.mkdir(opts.output)

# Creating WAV files with names for keys
#for count in range(opts.key_start, opts.key_stop+1):
def note(count, duration = opts.duration, freqsample = opts.freqsample, output = opts.output, verbose = opts.verbose):
    t, waveform = bh.waveform(count, duration, freqsample, verbose)
    
    wavdata = np.zeros(len(waveform), np.int32)
    waveform = waveform/np.max(waveform) * .98 * np.iinfo(np.int32).max
    wavdata[:] = waveform.astype(np.int32)

    wavfile = wave.open(f"{output}/{count:02}.wav", "wb")

    wavfile.setparams((1, 4, freqsample, len(waveform), "NONE", "Uncompressed"))
    wavfile.writeframesraw(wavdata.tobytes())

if __name__ == "__main__":
    opts = parse_commandline()
    print("Generating 88 .wav files")
    with Pool(8) as p:
        p.map(note, range(opts.key_start, opts.key_stop+1))
    print("Finished generating .wav files")

